	.file	1 "ud.c"
	.section .mdebug.abi32
	.previous
	.nan	legacy
	.module	fp=32
	.module	nooddspreg
	.globl	buf
	.data
	.align	2
	.type	buf, @object
	.size	buf, 180
buf:
	.word	1
	.word	1
	.word	2
	.word	3
	.word	5
	.word	8
	.word	13
	.word	21
	.word	34
	.word	55
	.word	89
	.word	144
	.word	233
	.word	377
	.word	610
	.word	987
	.word	1597
	.word	2584
	.word	4181
	.word	6765
	.word	10946
	.word	17711
	.word	28657
	.word	46368
	.word	75025
	.word	121393
	.word	196418
	.word	317811
	.word	514229
	.word	832040
	.word	1346269
	.word	2178309
	.word	3524578
	.word	5702887
	.word	9227465
	.word	14930352
	.word	24157817
	.word	39088169
	.word	63245986
	.word	102334155
	.word	165580141
	.word	267914296
	.word	433494437
	.word	701408733
	.space	4
	.globl	uart
	.section	.sdata,"aw",@progbits
	.align	2
	.type	uart, @object
	.size	uart, 4
uart:
	.word	1006633184

	.comm	r,50,4

	.comm	s,50,4
	.text
	.align	2
	.globl	proberx
	.set	nomips16
	.set	nomicromips
	.ent	proberx
	.type	proberx, @function
proberx:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	sw	$fp,4($sp)
	move	$fp,$sp
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,48($2)
	move	$sp,$fp
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	proberx
	.size	proberx, .-proberx
	.align	2
	.globl	probetx
	.set	nomips16
	.set	nomicromips
	.ent	probetx
	.type	probetx, @function
probetx:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	sw	$fp,4($sp)
	move	$fp,$sp
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,52($2)
	move	$sp,$fp
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	probetx
	.size	probetx, .-probetx
	.align	2
	.globl	iostat
	.set	nomips16
	.set	nomicromips
	.ent	iostat
	.type	iostat, @function
iostat:
	.frame	$fp,8,$31		# vars= 0, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-8
	sw	$fp,4($sp)
	move	$fp,$sp
	lui	$2,%hi(uart)
	lw	$2,%lo(uart)($2)
	nop
	lw	$2,0($2)
	move	$sp,$fp
	lw	$fp,4($sp)
	addiu	$sp,$sp,8
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	iostat
	.size	iostat, .-iostat
	.align	2
	.globl	ioctl
	.set	nomips16
	.set	nomicromips
	.ent	ioctl
	.type	ioctl, @function
ioctl:
	.frame	$fp,16,$31		# vars= 8, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-16
	sw	$fp,12($sp)
	move	$fp,$sp
	sw	$4,16($fp)
	lui	$2,%hi(uart)
	lw	$2,%lo(uart)($2)
	nop
	sw	$2,0($fp)
	lw	$2,0($fp)
	lw	$3,16($fp)
	nop
	sw	$3,0($2)
	nop
	move	$sp,$fp
	lw	$fp,12($sp)
	addiu	$sp,$sp,16
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	ioctl
	.size	ioctl, .-ioctl
	.align	2
	.globl	Getc
	.set	nomips16
	.set	nomicromips
	.ent	Getc
	.type	Getc, @function
Getc:
	.frame	$fp,32,$31		# vars= 8, regs= 2/0, args= 16, gp= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	move	$fp,$sp
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,48($2)
	nop
	blez	$2,$L9
	nop

	jal	disableInterr
	nop

	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,48($2)
	nop
	addiu	$3,$2,-1
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	sw	$3,48($2)
	lui	$2,%hi(Ud)
	lw	$3,%lo(Ud)($2)
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	addu	$2,$3,$2
	lbu	$2,8($2)
	nop
	sb	$2,16($fp)
	lui	$2,%hi(Ud)
	lw	$2,%lo(Ud)($2)
	nop
	addiu	$2,$2,1
	andi	$3,$2,0xf
	lui	$2,%hi(Ud)
	sw	$3,%lo(Ud)($2)
	jal	enableInterr
	nop

	b	$L10
	nop

$L9:
	li	$2,-1			# 0xffffffffffffffff
	sb	$2,16($fp)
$L10:
	lb	$2,16($fp)
	move	$sp,$fp
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	Getc
	.size	Getc, .-Getc
	.align	2
	.globl	Putc
	.set	nomips16
	.set	nomicromips
	.ent	Putc
	.type	Putc, @function
Putc:
	.frame	$fp,32,$31		# vars= 8, regs= 2/0, args= 16, gp= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	move	$fp,$sp
	move	$2,$4
	sb	$2,32($fp)
	jal	disableInterr
	nop

	lui	$2,%hi(uart)
	lw	$2,%lo(uart)($2)
	nop
	lw	$2,0($2)
	nop
	sll	$2,$2,1
	sll	$2,$2,24
	sra	$2,$2,24
	sra	$2,$2,7
	sll	$2,$2,24
	sra	$2,$2,24
	sw	$2,16($fp)
	lw	$2,16($fp)
	nop
	beq	$2,$0,$L13
	nop

	lui	$2,%hi(uart)
	lw	$2,%lo(uart)($2)
	lb	$3,32($fp)
	nop
	sw	$3,4($2)
	b	$L14
	nop

$L13:
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,52($2)
	nop
	blez	$2,$L15
	nop

	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,52($2)
	nop
	addiu	$3,$2,-1
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	sw	$3,52($2)
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$2,28($2)
	nop
	addiu	$2,$2,1
	andi	$3,$2,0xf
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	sw	$3,28($2)
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	lw	$3,28($2)
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	addu	$2,$3,$2
	lbu	$3,32($fp)
	nop
	sb	$3,32($2)
	b	$L14
	nop

$L15:
	li	$4,111			# 0x6f
	jal	to_stdout
	nop

$L14:
	jal	enableInterr
	nop

	nop
	move	$sp,$fp
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	Putc
	.size	Putc, .-Putc
	.align	2
	.globl	print_string
	.set	nomips16
	.set	nomicromips
	.ent	print_string
	.type	print_string, @function
print_string:
	.frame	$fp,32,$31		# vars= 8, regs= 2/0, args= 16, gp= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-32
	sw	$31,28($sp)
	sw	$fp,24($sp)
	move	$fp,$sp
	sw	$4,32($fp)
	sw	$0,16($fp)
	lw	$2,32($fp)
	nop
	beq	$2,$0,$L21
	nop

	b	$L18
	nop

$L19:
	lw	$2,16($fp)
	lw	$3,32($fp)
	nop
	addu	$2,$3,$2
	lb	$2,0($2)
	nop
	move	$4,$2
	jal	to_stdout
	nop

	lw	$2,16($fp)
	nop
	addiu	$2,$2,1
	sw	$2,16($fp)
$L18:
	lw	$2,16($fp)
	lw	$3,32($fp)
	nop
	addu	$2,$3,$2
	lb	$2,0($2)
	nop
	bne	$2,$0,$L19
	nop

	nop
$L21:
	nop
	move	$sp,$fp
	lw	$31,28($sp)
	lw	$fp,24($sp)
	addiu	$sp,$sp,32
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	print_string
	.size	print_string, .-print_string
	.rdata
	.align	2
$LC0:
	.ascii	": \000"
	.text
	.align	2
	.globl	print_stri
	.set	nomips16
	.set	nomicromips
	.ent	print_stri
	.type	print_stri, @function
print_stri:
	.frame	$fp,24,$31		# vars= 0, regs= 2/0, args= 16, gp= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-24
	sw	$31,20($sp)
	sw	$fp,16($sp)
	move	$fp,$sp
	sw	$4,24($fp)
	sw	$5,28($fp)
	lw	$4,24($fp)
	jal	print_string
	nop

	lui	$2,%hi($LC0)
	addiu	$4,$2,%lo($LC0)
	jal	print_string
	nop

	lw	$4,28($fp)
	jal	print
	nop

	li	$4,10			# 0xa
	jal	to_stdout
	nop

	nop
	move	$sp,$fp
	lw	$31,20($sp)
	lw	$fp,16($sp)
	addiu	$sp,$sp,24
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	print_stri
	.size	print_stri, .-print_stri
	.rdata
	.align	2
$LC1:
	.ascii	"nao eh numero!\000\000"
	.text
	.align	2
	.globl	stringtoi
	.set	nomips16
	.set	nomicromips
	.ent	stringtoi
	.type	stringtoi, @function
stringtoi:
	.frame	$fp,40,$31		# vars= 16, regs= 2/0, args= 16, gp= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	move	$fp,$sp
	sw	$4,40($fp)
	sw	$0,16($fp)
	lw	$2,40($fp)
	nop
	sw	$2,20($fp)
	lw	$2,20($fp)
	nop
	beq	$2,$0,$L24
	nop

	b	$L25
	nop

$L29:
	lw	$2,20($fp)
	nop
	lb	$2,0($2)
	nop
	slt	$2,$2,48
	bne	$2,$0,$L26
	nop

	lw	$2,20($fp)
	nop
	lb	$2,0($2)
	nop
	slt	$2,$2,58
	bne	$2,$0,$L27
	nop

$L26:
	lui	$2,%hi($LC1)
	addiu	$4,$2,%lo($LC1)
	jal	print_string
	nop

	b	$L28
	nop

$L27:
	lw	$2,16($fp)
	nop
	sll	$2,$2,1
	sll	$3,$2,2
	addu	$2,$2,$3
	sw	$2,16($fp)
	lw	$2,20($fp)
	nop
	lb	$3,0($2)
	li	$2,48			# 0x30
	bne	$2,$0,1f
	div	$0,$3,$2
	break	7
1:
	mfhi	$2
	sll	$2,$2,24
	sra	$2,$2,24
	move	$3,$2
	lw	$2,16($fp)
	nop
	addu	$2,$2,$3
	sw	$2,16($fp)
$L28:
	lw	$2,20($fp)
	nop
	addiu	$2,$2,1
	sw	$2,20($fp)
$L25:
	lw	$2,20($fp)
	nop
	lb	$2,0($2)
	nop
	bne	$2,$0,$L29
	nop

$L24:
	sw	$0,24($fp)
	b	$L30
	nop

$L31:
	lw	$3,16($fp)
	li	$2,10			# 0xa
	bne	$2,$0,1f
	div	$0,$3,$2
	break	7
1:
	mfhi	$2
	move	$3,$2
	lw	$2,24($fp)
	nop
	addu	$2,$2,$3
	sw	$2,24($fp)
	lw	$2,24($fp)
	nop
	sll	$2,$2,1
	sll	$3,$2,2
	addu	$2,$2,$3
	sw	$2,24($fp)
	lw	$3,16($fp)
	li	$2,10			# 0xa
	bne	$2,$0,1f
	div	$0,$3,$2
	break	7
1:
	mfhi	$2
	mflo	$2
	sw	$2,16($fp)
$L30:
	lw	$2,16($fp)
	nop
	bne	$2,$0,$L31
	nop

	lw	$3,24($fp)
	li	$2,10			# 0xa
	bne	$2,$0,1f
	div	$0,$3,$2
	break	7
1:
	mfhi	$2
	mflo	$2
	sw	$2,24($fp)
	lw	$2,24($fp)
	move	$sp,$fp
	lw	$31,36($sp)
	lw	$fp,32($sp)
	addiu	$sp,$sp,40
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	stringtoi
	.size	stringtoi, .-stringtoi
	.align	2
	.globl	nstringtoi
	.set	nomips16
	.set	nomicromips
	.ent	nstringtoi
	.type	nstringtoi, @function
nstringtoi:
	.frame	$fp,40,$31		# vars= 16, regs= 2/0, args= 16, gp= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-40
	sw	$31,36($sp)
	sw	$fp,32($sp)
	move	$fp,$sp
	sw	$4,40($fp)
	sw	$0,16($fp)
	sw	$0,24($fp)
	lw	$2,40($fp)
	nop
	sw	$2,20($fp)
	lw	$2,20($fp)
	nop
	beq	$2,$0,$L34
	nop

	b	$L35
	nop

$L39:
	lw	$2,20($fp)
	nop
	lb	$2,0($2)
	nop
	slt	$2,$2,48
	bne	$2,$0,$L36
	nop

	lw	$2,20($fp)
	nop
	lb	$2,0($2)
	nop
	slt	$2,$2,58
	bne	$2,$0,$L37
	nop

$L36:
	lui	$2,%hi($LC1)
	addiu	$4,$2,%lo($LC1)
	jal	print_string
	nop

	b	$L38
	nop

$L37:
	lw	$2,16($fp)
	nop
	sll	$2,$2,1
	sll	$3,$2,2
	addu	$2,$2,$3
	sw	$2,16($fp)
	lw	$2,20($fp)
	nop
	lb	$3,0($2)
	li	$2,48			# 0x30
	bne	$2,$0,1f
	div	$0,$3,$2
	break	7
1:
	mfhi	$2
	sll	$2,$2,24
	sra	$2,$2,24
	move	$3,$2
	lw	$2,16($fp)
	nop
	addu	$2,$2,$3
	sw	$2,16($fp)
$L38:
	lw	$2,20($fp)
	nop
	addiu	$2,$2,1
	sw	$2,20($fp)
$L35:
	lw	$2,20($fp)
	nop
	lb	$3,0($2)
	li	$2,10			# 0xa
	bne	$3,$2,$L39
	nop

$L34:
	lw	$2,16($fp)
	move	$sp,$fp
	lw	$31,36($sp)
	lw	$fp,32($sp)
	addiu	$sp,$sp,40
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	nstringtoi
	.size	nstringtoi, .-nstringtoi
	.align	2
	.globl	itostring
	.set	nomips16
	.set	nomicromips
	.ent	itostring
	.type	itostring, @function
itostring:
	.frame	$fp,16,$31		# vars= 8, regs= 1/0, args= 0, gp= 0
	.mask	0x40000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-16
	sw	$fp,12($sp)
	move	$fp,$sp
	sw	$4,16($fp)
	sw	$5,20($fp)
	sw	$0,0($fp)
	sw	$0,4($fp)
	b	$L42
	nop

$L43:
	lw	$3,16($fp)
	li	$2,10			# 0xa
	bne	$2,$0,1f
	div	$0,$3,$2
	break	7
1:
	mfhi	$2
	move	$3,$2
	lw	$2,0($fp)
	nop
	addu	$2,$2,$3
	sw	$2,0($fp)
	lw	$3,16($fp)
	li	$2,10			# 0xa
	bne	$2,$0,1f
	div	$0,$3,$2
	break	7
1:
	mfhi	$2
	mflo	$2
	sw	$2,16($fp)
	lw	$2,0($fp)
	nop
	sll	$2,$2,1
	sll	$3,$2,2
	addu	$2,$2,$3
	sw	$2,0($fp)
$L42:
	lw	$2,16($fp)
	nop
	bne	$2,$0,$L43
	nop

	lw	$3,0($fp)
	li	$2,10			# 0xa
	bne	$2,$0,1f
	div	$0,$3,$2
	break	7
1:
	mfhi	$2
	mflo	$2
	sw	$2,0($fp)
	b	$L44
	nop

$L45:
	lw	$3,0($fp)
	li	$2,10			# 0xa
	bne	$2,$0,1f
	div	$0,$3,$2
	break	7
1:
	mfhi	$2
	andi	$2,$2,0x00ff
	addiu	$2,$2,48
	andi	$2,$2,0x00ff
	sll	$3,$2,24
	sra	$3,$3,24
	lw	$2,20($fp)
	nop
	sb	$3,0($2)
	lw	$3,0($fp)
	li	$2,10			# 0xa
	bne	$2,$0,1f
	div	$0,$3,$2
	break	7
1:
	mfhi	$2
	mflo	$2
	sw	$2,0($fp)
	lw	$2,20($fp)
	nop
	addiu	$2,$2,1
	sw	$2,20($fp)
	lw	$2,4($fp)
	nop
	addiu	$2,$2,1
	sw	$2,4($fp)
$L44:
	lw	$2,0($fp)
	nop
	bne	$2,$0,$L45
	nop

	lw	$2,20($fp)
	nop
	sb	$0,0($2)
	lw	$2,4($fp)
	nop
	subu	$2,$0,$2
	lw	$3,20($fp)
	nop
	addu	$2,$3,$2
	sw	$2,20($fp)
	nop
	move	$sp,$fp
	lw	$fp,12($sp)
	addiu	$sp,$sp,16
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	itostring
	.size	itostring, .-itostring
	.align	2
	.globl	main
	.set	nomips16
	.set	nomicromips
	.ent	main
	.type	main, @function
main:
	.frame	$fp,72,$31		# vars= 48, regs= 2/0, args= 16, gp= 0
	.mask	0xc0000000,-4
	.fmask	0x00000000,0
	.set	noreorder
	.set	nomacro
	addiu	$sp,$sp,-72
	sw	$31,68($sp)
	sw	$fp,64($sp)
	move	$fp,$sp
	sb	$0,48($fp)
	sw	$0,20($fp)
	sw	$0,28($fp)
	lui	$2,%hi(Ud)
	addiu	$2,$2,%lo(Ud)
	li	$3,16			# 0x10
	sw	$3,52($2)
	jal	iostat
	nop

	sw	$2,52($fp)
	addiu	$2,$fp,44
	sw	$2,32($fp)
	lw	$3,52($fp)
	lw	$2,32($fp)
	nop
	sw	$3,0($2)
	lw	$2,44($fp)
	nop
	andi	$2,$2,0xff
	sw	$2,44($fp)
	lw	$2,44($fp)
	nop
	ori	$2,$2,0x80
	sw	$2,44($fp)
	lw	$3,44($fp)
	li	$2,-8			# 0xfffffffffffffff8
	and	$2,$3,$2
	ori	$2,$2,0x3
	sw	$2,44($fp)
	lw	$2,44($fp)
	nop
	ori	$2,$2,0x10
	sw	$2,44($fp)
	lw	$2,44($fp)
	nop
	ori	$2,$2,0x8
	sw	$2,44($fp)
	lw	$2,32($fp)
	nop
	lw	$2,0($2)
	nop
	move	$4,$2
	jal	ioctl
	nop

	li	$2,-1			# 0xffffffffffffffff
	sw	$2,16($fp)
$L52:
	nop
$L47:
	jal	proberx
	nop

	sltu	$2,$2,1
	andi	$2,$2,0x00ff
	sll	$2,$2,24
	sra	$2,$2,24
	sb	$2,56($fp)
	bne	$2,$0,$L47
	nop

	lbu	$2,48($fp)
	nop
	sb	$2,36($fp)
	jal	Getc
	nop

	sb	$2,48($fp)
	lw	$2,16($fp)
	nop
	addiu	$2,$2,1
	sw	$2,16($fp)
	lbu	$2,48($fp)
	nop
	sll	$3,$2,24
	sra	$3,$3,24
	lui	$2,%hi(r)
	addiu	$4,$2,%lo(r)
	lw	$2,16($fp)
	nop
	addu	$2,$4,$2
	sb	$3,0($2)
	lui	$2,%hi(r)
	addiu	$3,$2,%lo(r)
	lw	$2,16($fp)
	nop
	addu	$2,$3,$2
	lb	$3,0($2)
	li	$2,10			# 0xa
	bne	$3,$2,$L48
	nop

	li	$2,-1			# 0xffffffffffffffff
	sw	$2,16($fp)
	lui	$2,%hi(r)
	addiu	$4,$2,%lo(r)
	jal	nstringtoi
	nop

	sw	$2,24($fp)
	lui	$2,%hi(buf)
	lw	$3,24($fp)
	nop
	sll	$3,$3,2
	addiu	$2,$2,%lo(buf)
	addu	$2,$3,$2
	lw	$2,0($2)
	nop
	sw	$2,40($fp)
	lui	$2,%hi(s)
	addiu	$5,$2,%lo(s)
	lw	$4,40($fp)
	jal	itostring
	nop

	li	$2,1			# 0x1
	sw	$2,20($fp)
$L48:
	lw	$2,20($fp)
	nop
	beq	$2,$0,$L49
	nop

	sw	$0,24($fp)
	b	$L50
	nop

$L51:
	lui	$2,%hi(s)
	addiu	$3,$2,%lo(s)
	lw	$2,24($fp)
	nop
	addu	$2,$3,$2
	lb	$2,0($2)
	nop
	move	$4,$2
	jal	Putc
	nop

	lw	$2,24($fp)
	nop
	addiu	$2,$2,1
	sw	$2,24($fp)
$L50:
	lui	$2,%hi(s)
	addiu	$3,$2,%lo(s)
	lw	$2,24($fp)
	nop
	addu	$2,$3,$2
	lb	$2,0($2)
	nop
	bne	$2,$0,$L51
	nop

	li	$4,10			# 0xa
	jal	Putc
	nop

$L49:
	sw	$0,20($fp)
	lbu	$2,48($fp)
	nop
	sll	$2,$2,24
	sra	$2,$2,24
	bne	$2,$0,$L52
	nop

	lb	$3,36($fp)
	li	$2,10			# 0xa
	bne	$3,$2,$L52
	nop

	sw	$0,16($fp)
	b	$L53
	nop

$L54:
	lw	$2,16($fp)
	nop
	addiu	$2,$2,1
	sw	$2,16($fp)
$L53:
	lw	$2,16($fp)
	nop
	slt	$2,$2,100
	bne	$2,$0,$L54
	nop

	move	$2,$0
	move	$sp,$fp
	lw	$31,68($sp)
	lw	$fp,64($sp)
	addiu	$sp,$sp,72
	j	$31
	nop

	.set	macro
	.set	reorder
	.end	main
	.size	main, .-main
	.ident	"GCC: (GNU) 5.1.0"
