/* Trabalho Prático referente à disciplina CI067 - Software Básico 
 * Professor: Roberto Hexsel
 * Aluno: Paolo Andreas Stall Rechia
 * GRR: 20135196
 */

/* Comentarios de bloco em geral se referem ao código imediatamente abaixo
 */

#include <cMIPS.h>
#include "vetorFib.h"

#define EOF -1
#define NULL 0

/*Estruturas basicas*/

typedef struct control { // control register fields (uses only ls byte)
  int ign   : 24,        // ignore uppermost bits
    rts     : 1,         // Request to Send output (bit 7)
    ign2    : 2,         // bits 6,5 ignored
    intTX   : 1,         // interrupt on TX buffer empty (bit 4)
    intRX   : 1,         // interrupt on RX buffer full (bit 3)
    speed   : 3;         // 4,8,16..256 tx-rx clock data rates  (bits 0..2)
} Tcontrol;

typedef struct status {  // status register fields (uses only ls byte) unsigned 
  int ign : 24,          // ignore uppermost 3 bytes
  cts     : 1,           // Clear To Send input=1 (bit 7)
  txEmpty : 1,           // TX register is empty (bit 6)
  rxFull  : 1,           // octet available from RX register (bit 5)
  int_TX_empt: 1,        // interrupt pending on TX empty (bit 4)
  int_RX_full: 1,        // interrupt pending on RX full (bit 3)
  ign1    : 1,           // ignored (bit 2)
  framing : 1,           // framing error (bit 1)
  overun  : 1;           // overun error (bit 0)
} Tstatus;


typedef union ctlStat { // control + status on same address
  Tcontrol  ctl;        // write-only
  Tstatus   stat;       // read-only
} TctlStat;

typedef union data {    // data registers on same address
  int tx;               // write-only
  int rx;               // read-only
} Tdata;

typedef struct serial {
  TctlStat cs;
  Tdata    d;
} Tserial;


typedef  struct UARTdriver {
    int rx_hd;              // reception  queue  head  index
    int rx_tl;              //  reception  queue  tail  index
    char rx_q [16];         //  reception  queue
    int tx_hd;              //  transmission  queue  head  index
    int tx_tl;              //  transmission  queue  tail  index
    char tx_q [16];         //  transmission  queue
    int nrx;                //  number  of  characters  in  rx_queue
    int ntx;                //  number  of  spaces  in  tx_queue
    } UARTdriver;


/* var globais */

extern UARTdriver  Ud;      // driver
volatile Tserial *uart = (void *)IO_UART_ADDR;  //endereco da UART
char r[50];                 // string de recepcao
char s[50];                 // string de envio

/* prototipos */

// funcoes essenciais
int proberx(void);          //  retorna  nrx
int probetx(void);          //  retorna  ntx
int iostat(void);           //  retorna  inteiro  com  status  no byte  menos  sign
void ioctl(int);            //  escreve  byte  menos  sign no reg de  controle
char Getc(void);            //  rereception?torna  caractere  na fila , decrementa  nrx
void Putc(char);            //  insere  caractere  na fila , decrementa  ntx

// funcoes de manipulacao de string
void print_string(char *);          // imprime string (instavel! nao usar)
void print_stri(char *, int);       // idem, porem com um numero ao final
int nstringtoi(char *);             // transforma uma string terminada em \n
                                    // em inteiro
int stringtoi(char * string);       // o mesmo, para string terminada em \0
void itostring(int , char * );      // transforma inteiro em string


/* definicoes das funcoes */

int proberx(void){
    return Ud.nrx;
}
int probetx(void){
    return Ud.ntx;
}

int iostat(void){
    return *(int *)&uart->cs.stat;
}

void ioctl(int x){
    int * pter;
    pter = (int *) &uart->cs.ctl;
    *pter = x;
}

// Funcao retirada da apostila.
// Tira caracter da fila, decrementa nrx e incrementa rx_hd.
// Interrupcoes desativadas para evitar concorrencia com o tratador
char Getc( void ) {
    char c;
    if (Ud.nrx > 0) {
            disableInterr();      
            Ud.nrx  = Ud.nrx - 1;
            c = Ud.rx_q[Ud.rx_hd ];
            Ud.rx_hd = (Ud.rx_hd + 1) & 15;
            enableInterr();
        }
    else {
        c = EOF;
    }
    return c;
}

// Preenche a fila de transmissao
// Verifica se o RegTx esta vazio
// Se vazio, coloca novo caracter diretamente no registrador
// Se nao esta vazio, entao insere caracter na fila e ajusta ntx e tx_tl
void Putc( char c ) {
    disableInterr();
    int vazio;
    if (vazio = uart->cs.stat.txEmpty){
        uart->d.tx=c;
    }   
    else if (Ud.ntx > 0){
        Ud.ntx--;
        Ud.tx_tl = (Ud.tx_tl + 1) & 15;
        Ud.tx_q[Ud.tx_tl] = c;
    }
    else
        to_stdout('o'); 
    enableInterr();
}

// Imprime string -- criada para ajudar na depuracao porem
// deve ser evitada devido a instabilidade no uso.
// Quando utilizada inverte ordem das instrucoes.
//
// Por exemplo:
//
// print_string("uma string qualquer);
// print(i);
// vai resultar em:
// print(i);
// print_string("uma string qualquer);
void print_string(char * string){
    int i = 0;
    if (string != NULL){
        while (string[i] != '\0'){
            to_stdout(string[i]);
            i++;
        }
    }
    return;
}

// Imprime string + um número + \n
// mesmo problema da funcao acima
void print_stri(char * string, int i){
    print_string(string);
    print_string(": ");
    print(i);
    to_stdout('\n');
}


// Converte string termianda em \0 para inteiro.
// Nao funciona para multiplos de 10 e nao eh
// utilizada no programa principal
int stringtoi(char * string){
    int x = 0;
    char * aux = string;
    if (aux != NULL){
       while (*aux != '\0'){
           if (*aux < 48 || *aux > 57){
               print_string("nao eh numero!\0");
           }
           else{
               x = x * 10;
               x = x + (*aux % 48);
           }
       aux++;
       }
    }
    int y = 0;
    while (x != 0){
        y = y + x % 10;
        y = y * 10;
        x = x / 10;
    }
    y = y/10;
    return y;
}

// Funcao para string terminada em \n ao inves de \0,
// Como as que sao recebidas pela interface serial.
// Funciona corretamente apenas para numeros de 1-99.
// Uma maneira que pensei de contornar a limitacao dos multiplos de 10
// foi receber numeros em Octal, porque isso permite fazer a conversao
// direto do ASCII, seguida de um deslocamento de 3 bits para esquerda.
// Limitacao seria implementar a conversao Decimal -> Octal -> Decimal
int nstringtoi(char * string){
    int x = 0;
    int y = 0;
    char * aux = string;
    if (aux != NULL){
        while (*aux != '\n'){
            if (*aux < 48 || *aux > 57){
                print_string("nao eh numero!\0");
            }
            else{
                x = x * 10;
                x = x + (*aux % 48);
            }
       aux++;
       }
    }
    return x;
}

// inteiro para string
void itostring(int x, char * string){
    int y = 0;
    int i = 0;
    // inverte a ordem dos digitos de x e salva em y
    while (x != 0){
        y = y + x % 10;
        x = x /10;
        y = y * 10;
    }
    y = y / 10;

    // monta a string a partir do ultimo digito de y
    // (por isso deve ser invertido)
    while (y != 0){
        *string = (char) ((y%10) + 48);
        y = y / 10;
       *string++;
        i++;
    } 
    *string = '\0';
    string = string - i;
}


int main(){

    volatile Tcontrol control;
    volatile char c = 0;
    volatile int status;
    int * ctrl;
    volatile int * aux;
    int i;
    int send = 0;
    int fib, j;
    int x = 0;
    char last_c;
    volatile char teste;

    // inicia ntx para indicar que fila esta vazia
    Ud.ntx=16;

    // le status da UART
    status = iostat();

    ctrl = (int *) &control;
    * ctrl = status; 
    control.ign = 0;
    control.rts = 1;        // inicia envio
    control.speed = 3;      // velocidade minima, necessaria para rodar
    control.intTX = 1;      // interrupcoes de transmissao e recepcao
    control.intRX = 1;      // atividas

    // escreve mudancas no registrador de controle
    ioctl(*ctrl);

    // loop principal
    i = -1;
    do {
        // espera receber caracter
        while ( teste = proberx() == 0){};

        last_c = c;
        c = Getc();
        i++;
        r[i] = c;

        // quando acha um \n traduz a string encontrada
        // em inteiro, indexa o vetor de fibonacci e cria
        // uma string com o numero de fib
        if (r[i]==10){
            i = -1;
            j = nstringtoi(r);
            fib = buf[j];
            itostring(fib, s);
            send = 1;
        }

        // se gerou uma string de fib, enviar
        if (send){
            j = 0;
            while (s[j]!=0){
                Putc(s[j]); 
                j++;
            }
            // '\n' ao final para sinalizar termino de string
            Putc('\n');
        }
        send = 0;

        // o ultimo caracter enviado pela UART eh \0
        // isso permite a condicao do loop ser c != '\0'
        // no entanto impossibilita receber o indice 0
        // por isso o last_c eh utilizado

    } while (c != '\0' || last_c != '\n');

    // espera ultimos caracteres serem transmitidos
    i = 0;
    while (i < 100){
        i++;
        x * x;
    }
    return 0;
}
