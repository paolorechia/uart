    #################################################
    # Trabalho Pratico - CI067
    # Tratador de interrupcoes UART
    # Professor: Roberto Hexsel
    # Aluno: Paolo Andreas Stall Rechia
    # GRR: 20135196


    # Esse tratador tem duas funcoes:
    # Enfileirar caracteres recebidos em rx_q
    # Desenfileirar caracteres colocados em tx_q
    #################################################

    # uso dos registradores
    # k0 = _uart_buff
    # k1 = Status
    # a0 = HW_uart_addr
    # a1 = Ud
    # a2 = var temp2
    # a3 = var temp3

    #################################################

    # salva registradores adicionais

    sw    $a2, 7*4($k0)
    sw    $a3, 8*4($k0)

    lui   $a1, %hi(Ud)          # a1 <- &Ud
    ori   $a1, %lo(Ud)

    # confere codigo de interrupcao pendente

    andi  $a2, $k1, UART_rx_irq # Eh recepcao?
	beq   $a2, $zero, UARTtx    # Nao, confere transmissao
	nop                         # Branch-Delay-Slot

	# trata recepcao

    lw    $a3, 48($a1)          # a3 <- nrx
    nop
    addi  $a3, $a3, 1           # nrx++

    sw    $a3, 48($a1)          # Salva nrx

    lw    $a3, 4($a1)           # a3 <- rx_tl
    nop


    addi  $a2, $a1, 8           # a2 <- &rx_q

    add   $a2, $a3, $a2         # a2 <- &rx_q[rx_tl]


                                # Le RxReg da UART
	lb    $a3, 4($a0) 	        # a3 <- RxReg
	nop                          

	sb    $a3, 0($a2)           # E salva na cauda da fila


    lw    $a3, 4($a1)           # a3 <- rx_tl
    nop
    addi  $a3, $a3, 1           # rx_tl++
    andi  $a3, $a3, 15          # rx_tl % 16
    sw    $a3, 4($a1)           # Salva rx_tl

    # fim tratamento recepcao

    j UART_rec                  # salta para recomposicao de registradores

    nop

UARTtx:

	andi  $a2, $k1, UART_tx_irq # eh transmissao?
    beq   $a2, $zero, UART_rec   # nao? algo deu errado, pula fora
    nop

    # trata transmissao

    lw    $a3, 52($a1)          # Le ntx
    nop

    # testa se a fila estiver vazia --
    # neste caso nada a fazer, pula para recomposicao de registradores

    li    $a2, 16
    beq   $a3, $a2, UART_rec
    nop
    nop

    # do contrario realiza as operacoes necessarias

    addi  $a3, $a3, 1

    sw    $a3, 52($a1)          # Salva ntx incrementado
                                # Porque terminou de transmitir um char

    lw    $a3, 24($a1)          # Le tx_hd
    nop
    addi  $a3, $a3, 1           # tx_hd++
    andi  $a3, $a3, 15          # tx_hd % 16
    sw    $a3, 24($a1)          # Salva tx_hd


    addi  $a2, $a1, 32          # a2 <- &tx_q

    add   $a2, $a2, $a3         # a2 <- &tx_t[tx_hd]
                                # Calcula endereco da cabeca da fila

    lb    $a3, 0($a2)           # Le caracter da cabeca da fila
	nop                          
	sb    $a3, 4($a0) 	        # Salva em TxReg da UART para enviar

UART_rec:

    # recupera registradores adicionais
    lw $a2, 7*4($k0)
    lw $a3, 8*4($k0)
